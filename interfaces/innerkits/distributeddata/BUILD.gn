# Copyright (c) 2021 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import("//build/ohos.gni")

group("build_module") {
  deps = [ ":distributeddata_inner" ]
}

config("distributeddatafwk_config") {
  visibility = [ ":*" ]

  cflags = [ "-Wno-multichar" ]

  include_dirs = [
    "include",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/include",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src",
    "../../../frameworks/common",
    "../../../frameworks/innerkitsimpl/rdb/include",
    "../../../frameworks/innerkitsimpl/rdb/src",
    "../../../frameworks/innerkitsimpl/object/include",
    "../../../frameworks/innerkitsimpl/kvdb/include",
    "../../../frameworks/innerkitsimpl/kvdb/src",
    "//utils/system/safwk/native/include",
    "//utils/native/base/include",
    "//foundation/distributeddatamgr/appdatamgr/data_share/interfaces/inner_api/provider/include",
    "//foundation/distributeddatamgr/appdatamgr/data_share/interfaces/inner_api/common/include",
    "//foundation/distributeddatamgr/distributedfile/interfaces/kits/js/src/mod_securitylabel",
  ]
}

config("distributeddatafwk_public_config") {
  visibility = [ "//foundation/distributeddatamgr/distributeddatamgr:*" ]

  include_dirs = [
    "include",
    "../../../frameworks/innerkitsimpl/rdb/include",
    "../../../frameworks/innerkitsimpl/object/include",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/include",
    "//utils/native/base/include",
  ]
}

ohos_shared_library("distributeddata_inner") {
  old_sources = [
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src/blob.cpp",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src/change_notification.cpp",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src/data_query.cpp",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src/device_status_change_listener_client.cpp",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src/distributed_kv_data_manager.cpp",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src/idevice_status_change_listener.cpp",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src/ikvstore_client_death_observer.cpp",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src/ikvstore_data_service.cpp",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src/ikvstore_observer.cpp",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src/ikvstore_resultset.cpp",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src/ikvstore_single.cpp",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src/ikvstore_sync_callback.cpp",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src/itypes_util.cpp",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src/kv_utils.cpp",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src/kvstore_client_death_observer.cpp",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src/kvstore_datashare_bridge.cpp",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src/kvstore_observer_client.cpp",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src/kvstore_resultset_client.cpp",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src/kvstore_service_death_notifier.cpp",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src/kvstore_sync_callback_client.cpp",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src/single_kvstore_client.cpp",
    "../../../frameworks/innerkitsimpl/distributeddatafwk/src/sync_observer.cpp",
    "include/types.h",
  ]

  rdb_sources = [
    "../../../frameworks/innerkitsimpl/rdb/src/rdb_manager.cpp",
    "../../../frameworks/innerkitsimpl/rdb/src/rdb_manager_impl.cpp",
    "../../../frameworks/innerkitsimpl/rdb/src/rdb_notifier.cpp",
    "../../../frameworks/innerkitsimpl/rdb/src/rdb_service_proxy.cpp",
  ]
  object_sources = [
    "../../../frameworks/innerkitsimpl/object/src/iobject_callback.cpp",
    "../../../frameworks/innerkitsimpl/object/src/object_service_proxy.cpp",
  ]

  kvdb_sources = [
    "../../../frameworks/innerkitsimpl/kvdb/src/auto_sync_timer.cpp",
    "../../../frameworks/innerkitsimpl/kvdb/src/dev_manager.cpp",
    "../../../frameworks/innerkitsimpl/kvdb/src/device_store_impl.cpp",
    "../../../frameworks/innerkitsimpl/kvdb/src/kvdb_service_client.cpp",
    "../../../frameworks/innerkitsimpl/kvdb/src/observer_bridge.cpp",
    "../../../frameworks/innerkitsimpl/kvdb/src/security_manager.cpp",
    "../../../frameworks/innerkitsimpl/kvdb/src/single_store_impl.cpp",
    "../../../frameworks/innerkitsimpl/kvdb/src/store_factory.cpp",
    "../../../frameworks/innerkitsimpl/kvdb/src/store_manager.cpp",
    "../../../frameworks/innerkitsimpl/kvdb/src/store_result_set.cpp",
    "../../../frameworks/innerkitsimpl/kvdb/src/store_util.cpp",
    "../../../frameworks/innerkitsimpl/kvdb/src/system_api.cpp",
  ]

  sources = old_sources + rdb_sources + kvdb_sources + object_sources

  configs = [ ":distributeddatafwk_config" ]

  deps = [
    "//foundation/distributeddatamgr/distributeddatamgr/services/distributeddataservice/adapter:distributeddata_adapter",
    "//foundation/distributeddatamgr/distributeddatamgr/services/distributeddataservice/adapter/utils:distributeddata_utils_static",
    "//foundation/distributeddatamgr/distributeddatamgr/services/distributeddataservice/libs/distributeddb:distributeddb",
    "//utils/native/base:utils",
  ]
  external_deps = [
    "dataclassification:data_transit_mgr",
    "dsoftbus:softbus_client",
    "hitrace_native:libhitrace",
    "hiviewdfx_hilog_native:libhilog",
    "ipc:ipc_core",
    "samgr_standard:samgr_proxy",
  ]

  public_configs = [ ":distributeddatafwk_public_config" ]

  subsystem_name = "distributeddatamgr"
  part_name = "distributeddatamgr"
}
